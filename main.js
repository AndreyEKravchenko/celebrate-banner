const openPopup = document.getElementById('open_pop_up');
const closePopup = document.getElementById('close_pop_up');
const popUp = document.getElementById('pop_up');

popUp.addEventListener('click', function(e) {
    {        
        let date = new Date();
        date.setDate(date.getDate()+365);
        document.cookie = "mynewuser=John; expires=" + date.toUTCString();
        popUp.classList.remove('active')
    };
})

openPopup.addEventListener('click', function(e) {
    console.log("try open");
    e.preventDefault();
    popUp.classList.add('active');
})

closePopup.addEventListener('click', function() {
    let date = new Date();
    date.setDate(date.getDate()+365);
    document.cookie = "mynewuser=John; domain=.engproff.ru; path=/; expires=" + date.toUTCString();
    popUp.classList.remove('active')
})

document.addEventListener('keydown', function(e) {
    if( e.keyCode == 27 ){ // код клавиши Escape, но можно использовать e.key
        let date = new Date();
        date.setDate(date.getDate()+365);
        document.cookie = "mynewuser=John; expires=" + date.toUTCString();
        popUp.classList.remove('active')
    }
});